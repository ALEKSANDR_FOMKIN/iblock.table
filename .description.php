<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("COMPONENT_DESCRIPTION"),
    "ICON" => "/images/comp.gif",
    "CACHE_PATH" => "Y",
    "PATH" => array(
        "ID" => "mcart",
        'NAME' => GetMessage('PARTNER_NAME'),
        "CHILD" => array(
            "ID" => "table",
            "NAME" => GetMessage("GROUP_NAME"),
        )
    )
);
