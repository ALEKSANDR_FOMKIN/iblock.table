<?php

require_once __DIR__. "/../structures/Group.php";

class MonthPlans {

    /**
     * @var string Дата
     */
    private $_date;

    /**
     * @var string Менеджер
     */
    private $_manager;

    private $_managerSwaped;

    /**
     * @var int планы по группам
     */
    private $_plans;

    public function getDate() {
        return $this->_date;
    }

    public function getManager() {
        return $this->_manager;
    }

    public function getManagerSwaped() {
        return $this->_managerSwaped;
    }

    public function getPlans() {
        return $this->_plans;
    }

    const DATE_KEY = "DATA";
    const MANAGER_KEY = "MENEDZHER";

    /**
     * @param array $plan
     */
    public function __construct($plan) {
        $this->_date = $plan[self::DATE_KEY]['VALUE'];
        $this->_manager = $plan[self::MANAGER_KEY]['VALUE'];
        $this->_managerSwaped = $plan["swaped"]['VALUE'];

        $groups = array();

        foreach ($plan as $key => $value) {
            if($key === self::DATE_KEY || $key === self::MANAGER_KEY) {
                continue;
            }
            $groups[] = new Group($value['NAME'], $value['VALUE']);

        }

        $this->_plans = $groups;
    }
}