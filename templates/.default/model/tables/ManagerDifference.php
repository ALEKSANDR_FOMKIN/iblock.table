<?php

class ManagerDifference {

    private $_manager;
    private $_plans;

    public function getPlans() {
        return $this->_plans;
    }

    public function getManager() {
        return $this->_manager;
    }

    /**
     * @param $_manager
     * @param $_plans
     */
    public function __construct($_manager, $_plans) {
        $this->_manager = $_manager;
        $this->_plans = $_plans;
    }
}