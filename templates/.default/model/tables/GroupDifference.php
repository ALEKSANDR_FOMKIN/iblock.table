<?php

class GroupDifference {
    private $_group;
    private $_plan;
    private $_percent;

    public function getGroup() {
        return $this->_group;
    }

    public function getPlan() {
        return $this->_plan;
    }

    public function getPercent() {
        return $this->_percent;
    }

    /**
     * @param string $_group
     * @param ActualPlanned $_plan
     * @param float $_percent
     */
    public function __construct($_group, $_plan, $_percent) {
        $this->_group = $_group;
        $this->_plan = $_plan;
        $this->_percent = $_percent;
    }
}