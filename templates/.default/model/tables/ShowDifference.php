<?php
class ShowDifference {
    private $_manager;
    private $_summary;

    public function getManager() {
        return $this->_manager;
    }

    public function getSummary() {
        return $this->_summary;
    }

    public function __construct($_manager, $_summary) {
        $this->_manager = $_manager;
        $this->_summary = $_summary;
    }
}