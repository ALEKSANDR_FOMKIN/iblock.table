<?php
class Income {

    /**
     * @var string Дата прихода
     */
    private $_date;

    /**
     * @var string Плательщик
     */
    private $_payer;

    /**
     * @var int Сумма
     */
    private $_sum;

    /**
     * @var string Счет
     */
    private $_account;


    private $_managerSwaped;

    /**
     * @var string Менеджер
     */
    private $_manager;

    /**
     * @var string Товарная группа
     */
    private $_group;

    /**
     * @var bool Включать / не включать в факт продаж
     */
    private $_included;

    /**
     * @var string Выставка
     */
    private $_show;

    private $_new;

    /**
     * @return string
     */
    public function getDate() {
        return $this->_date;
    }

    /**
     * @return string
     */
    public function getPayer() {
        return $this->_payer;
    }

    /**
     * @return int
     */
    public function getSum() {
        return $this->_sum;
    }

    /**
     * @return string
     */
    public function getManager() {
        return $this->_manager;
    }

    public function getManagerSwaped() {
        return $this->_managerSwaped;
    }

    /**
     * @return string
     */
    public function getGroup() {
        return $this->_group;
    }

    /**
     * @return bool
     */
    public function isIncluded() {
        return $this->_included;
    }

    /**
     * @return string
     */
    public function getShow() {
        return $this->_show;
    }

    /**
     * @return string
     */
    public function getAccount() {
        return $this->_account;
    }

    public function getNew() {
        return $this->_new;
    }

    const DATE_KEY = "DATA_PRIKHODA";
    const PAYER_KEY = "PLATELSHCHIK";
    const SUM_KEY = "SUMMA";
    const ACCOUNT_KEY = "SCHET";
    const MANAGER_KEY = "MENEDZHER";
    const GROUP_KEY = "TOVARNAYA_GRUPPA";
    const INCLUDED_KEY = "VKLYUCHAT";
    const SHOW_KEY = "VYSTAVKA";
    const NEW_KEY = "NOVYE_KOMPANII";
    const MANAGER_SWAPED_KEY = "MENEDZHER_SWAPED";

    /**
     * @param array $income
     */
    public function __construct($income) {
        $this->_date = $income[self::DATE_KEY]['VALUE'];
        $this->_payer = $income[self::PAYER_KEY]['VALUE'];
        $this->_sum =  $income[self::SUM_KEY]['VALUE'];
        $this->_account = $income[self::ACCOUNT_KEY]['VALUE'];
        $this->_manager = $income[self::MANAGER_KEY]['VALUE'];
        $this->_group = $income[self::GROUP_KEY]['VALUE'];
        $this->_included = $income[self::INCLUDED_KEY]['VALUE'];
        $this->_show = $income[self::SHOW_KEY]['VALUE'];
        $this->_new = $income[self::NEW_KEY]['VALUE'];
        $this->_managerSwaped = $income[self::MANAGER_SWAPED_KEY]['VALUE'];
    }
}