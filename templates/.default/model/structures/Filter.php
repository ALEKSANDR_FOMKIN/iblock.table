<?php
class Filter {
    private $_groupFilter;
    private $_managerFilter;
    private $_showFilter;
    private $_dateFilter;

    public function getGroupFilter() {
        return $this->_groupFilter;
    }

    public function getManagerFilter() {
        return $this->_managerFilter;
    }

    public function getShowFilter() {
        return $this->_showFilter;
    }

    public function getDateFilter(){
        return $this->_dateFilter;
    }

    /**
     * @param array $_groupFilter
     * @param array $_managerFilter
     * @param array $_showFilter
     * @param array $_dateFilter
     */
    public function __construct($_groupFilter, $_managerFilter, $_showFilter, $_dateFilter) {
        $this->_groupFilter = $_groupFilter;
        $this->_managerFilter = $_managerFilter;
        $this->_showFilter = $_showFilter;
        $this->_dateFilter = $_dateFilter;
    }
}