<?php

/**
 * Хранит в себе информацию о поступлениях и количестве новых клиентов
 */
class IncomeClients {
    private $_incomes;
    private $_clients;

    /**
     * IncomeClients constructor.
     * @param $_incomes
     * @param $_clients
     */
    public function __construct($_incomes, $_clients) {
        $this->_incomes = $_incomes;
        $this->_clients = $_clients;
    }

    /**
     * @return float
     */
    public function getIncomes() {
        return $this->_incomes;
    }

    /**
     * @return float
     */
    public function getClients() {
        return $this->_clients;
    }

    public function increaseIncomes($increment) {
        $this->_incomes += $increment;
    }

    public function increaseClients($increment) {
        $this->_clients += $increment;
    }
}