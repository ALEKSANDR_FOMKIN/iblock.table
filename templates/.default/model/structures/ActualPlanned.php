<?php

/**
 * Хранит в себе информацию о запланированном значении некоторой величины и ее реальном значении.
 */
class ActualPlanned {

    /**
     * @var float Запланированное значение
     */
    private $_plan;

    /**
     * @var float Реальное значение
     */
    private $_actual;

    public function __construct($_plan, $_actual) {
        $this->_plan = $_plan;
        $this->_actual = $_actual;
    }

    /**
     * @return float Получает запланированое значение
     */
    public function getPlan(){
        return $this->_plan;
    }

    /**
     * @return float Получает реально значение
     */
    public function getActual(){
        return $this->_actual;
    }

    public function increaseActual($increment) {
        $this->_actual += $increment;
    }

    public function increasePlan($increment) {
        $this->_plan += $increment;
    }
}