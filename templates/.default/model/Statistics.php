<?php

require_once "tables/Income.php";
require_once "tables/MonthPlans.php";
require_once "tables/ManagerDifference.php";
require_once "tables/GroupDifference.php";
require_once "tables/ShowDifference.php";
require_once "structures/ActualPlanned.php";
require_once "structures/IncomeClients.php";

class Statistics {
    private $_incomes;
    private $_plans;
    private $_groups;
    private $_managers;
    private $_shows;
    private $_filter;

    public function __construct($incomes, $plans, $groups, $managers, $shows, $filter = null){
        $this->_incomes = $incomes;
        $this->_plans = $plans;
        $this->_groups = $groups;
        $this->_managers = $managers;
        $this->_shows = $shows;
        $this->_filter = $filter;

        usort($this->_managers, function ($a, $b) {
            return strcmp($a["LAST_NAME"], $b["LAST_NAME"]);
        });
    }

    public function getShowsHeader() {
        if(!is_null($this->_filter) && !empty($this->_filter->getShowFilter())) {
            return $this->_filter->getShowFilter();
        } else {
            return $this->_shows;
        }
    }

    public function getGroupsHeader() {
        $groups = array();
        if(!is_null($this->_filter) && !empty($this->_filter->getGroupFilter())) {
            $groups = $this->_filter->getGroupFilter();
        } else {
            $groups = $this->_groups;
        }

        $newGroups = array();

        $i = 0;
        foreach ($groups as $g) {
            if($i==2){
                $newGroups[] = 'Инструмент';
            }
            $newGroups[] = $g;
            $i++;
        }

        if(count($groups) == 2) {
            $newGroups[] = 'Инструмент';
        }

        return $newGroups;
    }

    public function getGroups() {
        return $this->_groups;
    }

    public function cmp($a, $b) {
        return strcmp($a->getManager(), $b->getManager());
    }

    public function GetNewClients() {
        $counter = 0;
        foreach ($this->_incomes  as $income){
            if (!is_null($this->_filter) && $this->_filter->getDateFilter() != null && $this->_filter->getDateFilter() !== '') {
                $date = DateTime::createFromFormat("d.m.Y", $income->getDate());
                $dateFilter = DateTime::createFromFormat('m.Y', $this->_filter->getDateFilter());

                if ($date->format('m.Y') === $dateFilter->format('m.Y')) {
                    if ($income->getNew() == 'Да') {
                        $counter++;
                    }
                }
            }
            else {
                if ($income->getNew() == 'Да') {
                    $counter++;
                }
            }
        }
        return $counter;
    }

    public function GetIndividualPlans() {
        $stats = array();

        foreach ($this->_managers as $manager) {
            if(!is_null($this->_filter) and !empty($this->_filter->getManagerFilter())) {
                if(!in_array(($manager['LAST_NAME'] . ' ' . $manager['NAME']), $this->_filter->getManagerFilter())) {
                    continue;
                }
            }

            $managerIncomes = array();

            foreach ($this->_incomes as $income) {
                if($income->getManager() === ($manager['NAME'] . ' ' . $manager['LAST_NAME'])) {
                    $managerIncomes[] = $income;
                }
            }
            $plans = array();

            $gibkaSUm = 0;
            $ketecSum = 0;
            $gibkaPlan = 0;
            $ketecPlan = 0;

            foreach ($this->getGroupsHeader() as $group) {
                if($group == 'Инструмент') {
                    continue;
                }

                $sum = 0;

                foreach ($managerIncomes as $income) {
                    if ($income->getGroup() == $group and $income->isIncluded() === 'Да') {
                        if (!is_null($this->_filter) && $this->_filter->getDateFilter() !== '') {
                            $date = DateTime::createFromFormat("d.m.Y", $income->getDate());
                            $dateFilter = DateTime::createFromFormat('m.Y', $this->_filter->getDateFilter());

                            if ($date->format('m.Y') === $dateFilter->format('m.Y')) {
                                $sum += $income->getSum();

                                if($group == 'KETEC') {
                                    $ketecSum += $income->getSum();
                                }
                                if($group == 'Гибка') {
                                    $gibkaSUm += $income->getSum();
                                }
                            }
                        } else {
                            $sum += $income->getSum();

                            if($group == 'KETEC') {
                                $ketecSum += $income->getSum();
                            }
                            if($group == 'Гибка') {
                                $gibkaSUm += $income->getSum();
                            }
                        }
                    }
                }

                $plannedSum = 0;

                foreach ($this->_plans as $plan) {
                    if($plan->getManager() === ($manager['NAME'] . ' ' . $manager['LAST_NAME'])) {
                        foreach ($plan->getPlans() as $cell) {
                            if($cell->getName() === $group) {

                                if (!is_null($this->_filter) && $this->_filter->getDateFilter() !== '') {
                                    $date = DateTime::createFromFormat("d.m.Y", $plan->getDate());
                                    $dateFilter = DateTime::createFromFormat('m.Y', $this->_filter->getDateFilter());

                                    if ($date->format('m.Y') === $dateFilter->format('m.Y')) {
                                        $plannedSum += $cell->getValue();
                                        if($group == 'KETEC') {
                                            $ketecPlan += $cell->getValue();
                                        }
                                        if($group == 'Гибка') {
                                            $gibkaPlan += $cell->getValue();
                                        }
                                    }
                                } else {
                                    $plannedSum += $cell->getValue();
                                    if($group == 'KETEC') {
                                        $ketecPlan += $cell->getValue();
                                    }
                                    if($group == 'Гибка') {
                                        $gibkaPlan += $cell->getValue();
                                    }
                                }
                            }
                        }
                    }
                }

                $plans[] = new ActualPlanned($plannedSum, $sum);
            }

            array_splice( $plans, 2, 0, [new ActualPlanned($gibkaPlan + $ketecPlan, $gibkaSUm + $ketecSum)]);
            $stats[] = new ManagerDifference($manager, $plans);
        }

        $first = true;
        $summaryPlans = array();

        foreach ($stats as $stat) {
            $currentStat = $stat->getPlans();
            if($first) {
                foreach ($currentStat as $key => $value) {
                    $summaryPlans[] = clone $value;
                }
                $first = false;
                continue;
            } else {
                foreach ($currentStat as $key => $value) {
                    $summaryPlans[$key]->increaseActual($value->getActual());
                    $summaryPlans[$key]->increasePlan($value->getPlan());
                }
            }
        }

        if($summaryPlans != null) {
            $summary['NAME'] = 'Итого';
            $stats[] = new ManagerDifference($summary, $summaryPlans);
        }

        return $stats;
    }

    public function GetGroupPlans() {

        $incomes = array();

        foreach ($this->_incomes as $income) {
            if($income->getManager() !== 'НЕ НАЙДЕН') {
                $incomes[] = $income;
            }
        }

        $filteredIncomes = array();

        if(!is_null($this->_filter)) {
            if(!empty($this->_filter->getGroupFilter())) {
                    foreach ($incomes as $income) {
                        if (in_array($income->getGroup(), $this->_filter->getGroupFilter())) {
                            $filteredIncomes[] = $income;
                        }
                    }
            } else {
                $filteredIncomes = $this->_incomes;
            }
        } else {
            $filteredIncomes = $this->_incomes;
        }

        if(is_null($this->_filter) || empty($this->_filter->getGroupFilter())) {
            $filteredIncomes = $incomes;
        }

        $stats = array();

        $groups = $this->_groups;
        if(!is_null($this->_filter)) {
            if(!empty($this->_filter->getGroupFilter())) {
                $groups = $this->_filter->getGroupFilter();
            }
        }

        $summaryFact = 0;
        $summaryPlan = 0;

        foreach($groups as $group) {

            $fact = 0;
            foreach ($filteredIncomes as $income){

                if(!is_null($this->_filter) and !empty($this->_filter->getManagerFilter())) {
                    if(!in_array($income->getManagerSwaped(), $this->_filter->getManagerFilter())) {
                        continue;
                    }
                }

                if($income->getGroup() === $group and $income->isIncluded() === 'Да') {
                    if(!is_null($this->_filter) && $this->_filter->getDateFilter() !== '') {
                        $date = DateTime::createFromFormat("d.m.Y", $income->getDate());
                        $dateFilter = DateTime::createFromFormat('m.Y', $this->_filter->getDateFilter());

                        if($date->format('m.Y') === $dateFilter->format('m.Y')) {
                            $fact += $income->getSum();
                            if($group == 'KETEC' || $group == 'Гибка') {
                                $summaryFact += $income->getSum();
                            }
                        }
                    } else {
                        $fact += $income->getSum();
                        if($group == 'KETEC' || $group == 'Гибка') {
                            $summaryFact += $income->getSum();
                        }
                    }
                }
            }

            $plannedSum = 0;

            foreach ($this->_plans as $plan) {

                if(!is_null($this->_filter) and !empty($this->_filter->getManagerFilter())) {
                    if(!in_array($plan->getManagerSwaped(), $this->_filter->getManagerFilter())) {
                        continue;
                    }
                }

                foreach ($plan->getPlans() as $cell) {
                    if($cell->getName() === $group) {


                        if(!is_null($this->_filter) && $this->_filter->getDateFilter() !== '') {
                            $date = DateTime::createFromFormat("d.m.Y", $plan->getDate());
                            $dateFilter = DateTime::createFromFormat('m.Y', $this->_filter->getDateFilter());

                            if($date->format('m.Y') === $dateFilter->format('m.Y')) {
                                $plannedSum += $cell->getValue();

                                if($group == 'KETEC' || $group == 'Гибка') {
                                    $summaryPlan += $cell->getValue();
                                }
                            }
                        } else {
                            $plannedSum += $cell->getValue();

                            if($group == 'KETEC' || $group == 'Гибка') {
                                $summaryPlan += $cell->getValue();
                            }
                        }
                    }
                }
            }

            $plannedFloat  = floatval($plannedSum);
            $factFloat = floatval($fact);
            $percent = 0.0;
            if($plannedFloat != 0) {
                $percent =  $factFloat / $plannedFloat;
            }

            $percent = number_format((float)$percent * 100.0, 2, '.', '');

            $stats[] = new GroupDifference($group, new ActualPlanned($plannedSum , $fact), $percent);
        }

        $summaryPercent = 0.0;
        $summaryPlanFloat = floatval($summaryPlan);
        $summaryFactFloat = floatval($summaryFact);
        if($summaryPlanFloat != 0) {
            $summaryPercent = $summaryFactFloat / $summaryPlanFloat;
        }

        $summaryPercent = number_format((float)$summaryPercent * 100.0, 2, '.', '');
        $summaryGroup = new GroupDifference("Итого", new ActualPlanned($summaryPlan , $summaryFact), $summaryPercent);

        array_splice($stats, 2, 0, [$summaryGroup]);
        return $stats;
    }

    public function GetShowPlans() {
        $stats = array();

        foreach ($this->_managers as $manager) {
            if(!is_null($this->_filter) and !empty($this->_filter->getManagerFilter())) {
                if(!in_array(($manager['LAST_NAME'] . ' ' . $manager['NAME']), $this->_filter->getManagerFilter())) {
                    continue;
                }
            }

            $managerIncomes = array();

            foreach ($this->_incomes as $income) {
                if($income->getManager() === ($manager['NAME'] . ' ' . $manager['LAST_NAME'])) {
                    $managerIncomes[] = $income;
                }
            }

            if (empty($managerIncomes)) {
                continue;
            }

            $plans = array();

            foreach ($this->getShowsHeader() as $show) {
                $sum = 0;

                foreach ($managerIncomes as $income) {
                    if($income->getShow() === $show) {
                        if(!is_null($this->_filter) && $this->_filter->getDateFilter() !== '') {
                            $date = DateTime::createFromFormat("d.m.Y", $income->getDate());
                            $dateFilter = DateTime::createFromFormat('m.Y', $this->_filter->getDateFilter());

                            if($date->format('m.Y') === $dateFilter->format('m.Y')) {
                                $sum += $income->getSum();
                            }
                        } else {
                            $sum += $income->getSum();
                        }
                    }
                }

                $plans[] = new IncomeClients($sum, 0);
            }
            $stats[] = new ShowDifference($manager, $plans);
        }

        $first = true;
        $summaryPlans = array();

        foreach ($stats as $stat) {
            $currentStat = $stat->getSummary();
            if($first) {
                foreach ($currentStat as $key => $value) {
                    $summaryPlans[] = clone $value;
                }
                $first = false;
                continue;
            } else {
                foreach ($currentStat as $key => $value) {
                    $summaryPlans[$key]->increaseIncomes($value->getIncomes());
                }
            }
        }

        if($summaryPlans != null) {
            $summary['NAME'] = 'Итого';
            $stats[] = new ShowDifference($summary, $summaryPlans);
        }
        return $stats;
    }
}
