<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arFilter = array("IBLOCK_ID" => $arParams["INCOMES_IBLOCK_ID"]);
$incomesRaw = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, array());

while($row = $incomesRaw->GetNextElement()) {
    $arResult['INCOMES'][] = $row->GetProperties();
}

$arFilter2 = array("IBLOCK_ID" => $arParams["PLANS_IBLOCK_ID"]);
$plansRaw = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter2, false, false, array());

$users = CUser::GetList($by="", $order="", array("GROUPS_ID"=>array($arParams["MANAGER_GROUP_ID"])));

while($user = $users->Fetch()) {
    $arResult['MANAGERS'][] = array('NAME' => $user['NAME'], 'LAST_NAME' => $user['LAST_NAME'], 'ID' => $user['ID']);
}

$arResult['PLAN_NAMES'] = array();
$names_found = false;

while($row2 = $plansRaw->GetNextElement()) {
    $properties = $row2->GetProperties();
    $arResult['PLANS'][] = $properties;

    if(!$names_found) {
        foreach ($properties as $prop) {
            if($prop['NAME'] == "Дата" || $prop['NAME'] == "Менеджер") {
                continue;
            }
            $arResult['PLAN_NAMES'][] = $prop['NAME'];
        }
        $names_found = true;
    }
}

$goodsEnum = CIBlockPropertyEnum::GetList(
    array("DEF" => "DESC", "SORT" => "ASC"),
    array("IBLOCK_ID" => $arParams["INCOMES_IBLOCK_ID"], "CODE" => "TOVARNAYA_GRUPPA_"));

while($enum_fields = $goodsEnum->GetNext())  {
    $arResult['GROUPS'][] = $enum_fields["VALUE"];
}

$showEnum = CIBlockPropertyEnum::GetList(
    array("DEF" => "DESC", "SORT" => "ASC"),
    array("IBLOCK_ID" => $arParams["INCOMES_IBLOCK_ID"], "CODE" => "VYSTAVKA_"));

while($enum_fields = $showEnum->GetNext())  {
    $arResult['SHOW'][] = $enum_fields["VALUE"];
}

$this->IncludeComponentTemplate();