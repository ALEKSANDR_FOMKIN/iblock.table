<?php
global $APPLICATION;
global $USER;
$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
$sheet = $excel->getActiveSheet();
$sheet->setTitle('Ведение приходов средств');

$excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Дата прихода");
$excel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Плательщик");
$excel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, "Сумма");
$excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, "Счет");
$excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, "Менеджер");
$excel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, "Товарная группа");
$excel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, "Включать в факт продаж");
$excel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, "Выставка");

$sheet->getStyle('C2:C100')->getNumberFormat()->setFormatCode('# ##0.00');

$rowIndex = 2;
$arGroups = CUser::GetUserGroup($user_id);
if(in_array($arParams["MANAGER_GROUP_ID"], $arGroups)&& $arParams["ALLOW_VIEWING"] == "N"){
    foreach ($incomes as $income)  {
        if($income->getManagerSwaped() == $USER->GetLastName()." ".$USER->GetFirstName()){
            $excel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIndex, $income->getDate());
            $excel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIndex, $income->getPayer());
            $excel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowIndex, floatval($income->getSum()));
            $excel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowIndex, $income->getAccount());
            $excel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowIndex, $income->getManagerSwaped());
            $excel->getActiveSheet()->setCellValueByColumnAndRow(5, $rowIndex, $income->getGroup());
            $excel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIndex, $income->isIncluded());
            $excel->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIndex, $income->getShow());
            $rowIndex++;
        }
        else{
            continue;
        }
    }
}
else{
    foreach ($incomes as $income){
        $excel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIndex, $income->getDate());
        $excel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIndex, $income->getPayer());
        $excel->getActiveSheet()->setCellValueByColumnAndRow(2, $rowIndex, floatval($income->getSum()));
        $excel->getActiveSheet()->setCellValueByColumnAndRow(3, $rowIndex, $income->getAccount());
        $excel->getActiveSheet()->setCellValueByColumnAndRow(4, $rowIndex, $income->getManagerSwaped());
        $excel->getActiveSheet()->setCellValueByColumnAndRow(5, $rowIndex, $income->getGroup());
        $excel->getActiveSheet()->setCellValueByColumnAndRow(6, $rowIndex, $income->isIncluded());
        $excel->getActiveSheet()->setCellValueByColumnAndRow(7, $rowIndex, $income->getShow());
        $rowIndex++;
    }
}
$APPLICATION->RestartBuffer();

ob_end_clean();
header( "Content-type: application/vnd.ms-excel" );
header('Content-Disposition: attachment; filename="Ведение приходов средств.xlsx"');
header("Pragma: no-cache");
header("Expires: 0");
ob_end_clean();

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$writer->save('php://output');
exit();