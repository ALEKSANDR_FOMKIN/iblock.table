<?php
global $APPLICATION;

$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
$sheet = $excel->getActiveSheet();
$sheet->setTitle('План по товарным группам');

$sheet->setCellValueByColumnAndRow(0, 1, "Товарная группа");
$sheet->setCellValueByColumnAndRow(1, 1, "План");
$sheet->setCellValueByColumnAndRow(2, 1, "Факт");
$sheet->setCellValueByColumnAndRow(3, 1, "Выполнено");

$sheet->getColumnDimension('B')->setWidth(15);
$sheet->getColumnDimension('C')->setWidth(15);
$sheet->getColumnDimension('D')->setWidth(20);

global $USER;
$user_id = $USER->GetID();
$last_name = $USER->GetLastName();
$name = $USER->GetFirstName();
$arGroups = CUser::GetUserGroup($user_id);
if(in_array($arParams["MANAGER_GROUP_ID"], $arGroups)) {
    $filter = new Filter(array(), array($last_name ." ". $name), array(), '');
}
$statistics = new Statistics($incomes, $plans, $arResult['GROUPS'], $arResult['MANAGERS'], $arResult['SHOW'], $filter);

$rowIndex = 2;
foreach ($statistics->GetGroupPlans() as $groupPlan)  {
    $sheet->setCellValueByColumnAndRow(0, $rowIndex, $groupPlan->getGroup());
    $sheet->setCellValueByColumnAndRow(1, $rowIndex, floatval($groupPlan->getPlan()->getPlan()));
    $sheet->setCellValueByColumnAndRow(2, $rowIndex, floatval($groupPlan->getPlan()->getActual()));
    $sheet->setCellValueByColumnAndRow(3, $rowIndex, floatval($groupPlan->getPercent() / 100.0));
    $rowIndex++;
}

$sheet->getStyle('B2:B100')->getNumberFormat()->setFormatCode('# ##0.00');
$sheet->getStyle('C2:C100')->getNumberFormat()->setFormatCode('# ##0.00');
$sheet->getStyle('D2:D100')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00);

$APPLICATION->RestartBuffer();

ob_end_clean();
header( "Content-type: application/vnd.ms-excel" );
header('Content-Disposition: attachment; filename="План по товарным группам за ' . $filter->getDateFilter()  . '.xlsx"');
header("Pragma: no-cache");
header("Expires: 0");
ob_end_clean();

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$writer->save('php://output');
exit();