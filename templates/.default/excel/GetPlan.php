<?php
global $APPLICATION;

$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
$sheet = $excel->getActiveSheet();
$sheet->setTitle('Постановка ежемесячных планов');

$excel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Дата");
$excel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Менеджер");

$colIndex = 2;
foreach ($arResult['PLAN_NAMES'] as $plan) {
    $sheet->setCellValueByColumnAndRow($colIndex, 1, $plan);
    $colIndex += 1;
}

$rowIndex = 2;
foreach ($plans as $plan) {
    $excel->getActiveSheet()->setCellValueByColumnAndRow(0, $rowIndex, $plan->getDate());
    $excel->getActiveSheet()->setCellValueByColumnAndRow(1, $rowIndex, $plan->getManager());
    $colIndex = 2;
    foreach ($plan->getPlans() as $item) {
        $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $item->getValue());
        $colIndex +=1;
    }
    $rowIndex++;
}


ob_end_clean();
header( "Content-type: application/vnd.ms-excel" );
header('Content-Disposition: attachment; filename="Постановка ежемесячных планов за ' . $filter->getDateFilter() .  '.xlsx"');
header("Pragma: no-cache");
header("Expires: 0");
ob_end_clean();

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$writer->save('php://output');
exit();