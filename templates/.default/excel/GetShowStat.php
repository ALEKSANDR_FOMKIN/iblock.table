<?php
global $APPLICATION;

$statistics->GetIndividualPlans();

$excel = new PHPExcel();
$excel->setActiveSheetIndex(0);
$sheet = $excel->getActiveSheet();
$sheet->setTitle('Выполнения плана по выставкам');

$sheet->setCellValueByColumnAndRow(0, 1, "Менеджер");

$colIndex = 1;
foreach ($statistics->getShowsHeader() as $show) {
    $sheet->setCellValueByColumnAndRow($colIndex, 1, 'Поступления ' . $show);
    $sheet->setCellValueByColumnAndRow($colIndex + 1, 1, 'Клиенты ' .$show);
    $colIndex +=2;
}

$rowIndex = 2;
foreach ($statistics->GetShowPlans() as $showPlan) {
    $sheet->setCellValueByColumnAndRow(0, $rowIndex, $showPlan->getManager()['LAST_NAME'] . ' ' . $showPlan->getManager()['NAME']);
    $colIndex = 1;
    foreach ($showPlan->getSummary() as $summary) {

        $col1 = PHPExcel_Cell::stringFromColumnIndex($colIndex);
        $col2 = PHPExcel_Cell::stringFromColumnIndex($colIndex + 1);

        $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, floatval($summary->getIncomes()));
        $sheet->setCellValueByColumnAndRow($colIndex + 1, $rowIndex, floatval($summary->getClients()));
        $colIndex +=2;

        $sheet->getStyle($col1 . "2:" . $col1 . "100")->getNumberFormat()->setFormatCode('# ##0.00');
        $sheet->getStyle($col2 . "2:" . $col2 . "100")->getNumberFormat()->setFormatCode('# ##0.00');

        $sheet->getColumnDimension($col1)->setWidth(15);
        $sheet->getColumnDimension($col2)->setWidth(15);
    }
    $rowIndex++;
}

$APPLICATION->RestartBuffer();

ob_end_clean();
header( "Content-type: application/vnd.ms-excel" );
header('Content-Disposition: attachment; filename="Выполнения плана по выставкам за ' . $filter->getDateFilter() .  '.xlsx"');
header("Pragma: no-cache");
header("Expires: 0");
ob_end_clean();

$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$writer->save('php://output');
exit();