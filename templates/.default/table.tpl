<form name="table_form" method="POST" action = <?= $APPLICATION->GetCurPage() ?>>
    <div class="container-fluid">
        <div id="filters" class="well well-sm">

            <div class="filter-container">
                <h5>Фильтр по группам</h5>
                <select name="group_filter[]" id="group-list" multiple title="Выберите товарную группу"></select>
            </div>

            <div class="filter-container">
                <h5>Фильтр по менеджерам</h5>
                <select name="manager_filter[]" id="manager-list" multiple title="Выберите менеджеров"></select>
            </div>

            <div class="filter-container">
                <h5>Фильтр по выставкам</h5>
                <select name="show_filter[]" id="show-list" multiple title="Выберите выставку"></select>
            </div>

            <div class="filter-container">
                <h5>Фильтр по месяцу</h5>
                <label><input readonly name='date_filter' id="month-selector"></label>
                <br/>
                <button id="submit-button" name="update_page" onClick="window.location.reload()">
                    Обновить
                    <span class="fa fa-refresh fa-fw fa-1x"></span>
                </button>
            </div>

        </div>

        <? if($arResult['DEVELOPER_MODE'] === true): ?>
            ВЕРСИЯ ДЛЯ РАЗРАБОТЧИКОВ
            <h3>Ведение приходов средств</h3>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Дата прихода</th>
                        <th>Плательщик</th>
                        <th>Сумма</th>
                        <th>Счет</th>
                        <th>Менеджер</th>
                        <th>Товарная группа</th>
                        <th>Включать в факт продаж</th>
                        <th>Выставка</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach ($incomes as $income): ?>
                        <tr>
                            <td><?= $income->getDate() ?></td>
                            <td><?= $income->getPayer() ?></td>
                            <td><?= $income->getSum() ?></td>
                            <td><?= $income->getAccount() ?></td>
                            <td><?= $income->getManager() ?></td>
                            <td><?= $income->getGroup() ?></td>
                            <td><?= $income->isIncluded() ?></td>
                            <td><?= $income->getShow() ?></td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>

            <button id="download-incomes" type="submit" name="get_incomes" class="btn btn-success">
                Скачать
                <i class="fa fa-download fa-fw fa-1x"></i>
            </button>
            <hr />

            <h3>Постановка ежемесячных планов</h3>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Дата</th>
                        <th>Менеджер</th>

                        <?foreach ($arResult['PLAN_NAMES'] as $item) : ?>
                            <th><?= $item ?></th>
                        <?endforeach;?>
                    </tr>
                    </thead>
                    <tbody>
                    <?foreach ($plans as $plan): ?>
                        <tr>
                            <td><?= $plan->getDate() ?></td>
                            <td><?= $plan->getManager() ?></td>

                            <?foreach ($plan->getPlans() as $item) :?>
                                <td> <?=$item->getValue() ?> </td>
                            <?endforeach;?>

                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
            <button id="download-plans" name="get_plans" type="submit" class="btn btn-success">
                Скачать
                <i class="fa fa-download fa-fw fa-1x"></i>
            </button>
            <hr />
            <button name="get_incomes" type="submit" class="btn btn-success">
                Выгрузить приходы
                <i class="fa fa-download fa-fw fa-1x"></i>
            </button>
        <? endif; ?>

        <p>
            Новых клиентов: <? if($statistics->getNewClients()  != null): print($statistics->getNewClients()); endif;?>
        </p>
        <hr />
        <h3>План/факт по выбранным направлениям за <?= $dateFilter ?></h3>
        <div class="table-responsive">
            <table class="table table-bordered summary-table">
                <thead>
                <tr>
                    <th class="top center">Менеджер</th>
                    <? foreach ($statistics->getGroupsHeader() as $group): ?>
                        <th class="top center">План <?= $group?></th>
                        <th class="top center">Факт <?= $group?></th>
                    <?endforeach;?>
                </tr>
                </thead>
                <tbody>
                <? foreach ($statistics->GetIndividualPlans() as $individualPlan): ?>
                    <tr>
                        <td> <?= $individualPlan->getManager()['LAST_NAME'] . ' ' . $individualPlan->getManager()['NAME'] ?> </td>

                        <? foreach ($individualPlan->getPlans() as $difference): ?>
                            <td class="right"> <?= number_format($difference->getPlan(), 2, ',', ' ') ?> </td>
                            <td class="right"> <?= number_format($difference->getActual(), 2, ',', ' ') ?> </td>
                        <? endforeach ?>

                    </tr>
                <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <button name="get_plan_fact_manager" type="submit" class="btn btn-success">
            Скачать
            <i class="fa fa-download fa-fw fa-1x"></i>
        </button>
        <hr />

        <h3>Выполнение плана по товарным группам за <?= $dateFilter ?></h3>
        <div class="table-responsive">
            <table class="table table-bordered before-summary-table">
                <thead>
                    <tr>
                        <th class="top center">Товарная группа</th>
                        <th class="top center">План</th>
                        <th class="top center">Факт</th>
                        <th class="top center">Выполнено</th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach ($statistics->GetGroupPlans() as $groupPlan): ?>
                        <tr class="<? if($groupPlan->getGroup() == 'Итого'): echo 'summary'; endif;?>">
                            <td><?= $groupPlan->getGroup() ?></td>
                            <td class="right"><?= number_format($groupPlan->getPlan()->getPlan(), 2, ',', ' ') ?></td>
                            <td class="right"><?= number_format($groupPlan->getPlan()->getActual(), 2, ',', ' ') ?></td>
                            <td class="right"><?= $groupPlan->getPercent() . '%' ?></td>
                        </tr>
                    <? endforeach; ?>
                </tbody>
            </table>
        </div>
        <button name="get_plan_fact_group" type="submit" class="btn btn-success">
            Скачать
            <i class="fa fa-download fa-fw fa-1x"></i>
        </button>
        <hr />

        <h3>Выполнение плана по выставкам за <?= $dateFilter ?> </h3>
        <div class="table-responsive">
            <table class="table table-bordered summary-table">
                <thead>
                    <tr>
                        <th class="top center">Менеджер</th>
                        <? foreach ($statistics->getShowsHeader() as $show): ?>
                            <th class="top center"> Поступления <?= $show ?> </th>
                            <th class="top center"> Клиенты <?= $show ?> </th>
                        <? endforeach ?>
                    </tr>
                </thead>
                <tbody>
                <? foreach ($statistics->GetShowPlans() as $showPlan): ?>
                    <tr>
                        <td> <?= $showPlan->getManager()['LAST_NAME'] . ' ' . $showPlan->getManager()['NAME'] ?> </td>

                        <? foreach ($showPlan->getSummary() as $summary): ?>
                            <td class="right"> <?= number_format($summary->getIncomes(), 2, ',', ' ') ?> </td>
                            <td class="right"> <?= number_format($summary->getClients(), 2, ',', ' ') ?> </td>
                        <?endforeach;?>

                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
        <button name="get_plan_fact_show" type="submit" class="btn btn-success">
            Скачать
            <i class="fa fa-download fa-fw fa-1x"></i>
        </button>
        <hr />

    </div>
</form>
<?global $USER;
$user_id = $USER->GetID();
$last_name = $USER->GetLastName();
$name = $USER->GetFirstName();
$arGroups = CUser::GetUserGroup($user_id);?>
<script>
    $(document).ready(function () {

        <? foreach ($arResult['GROUPS'] as $group): ?>
            <? if(in_array($group, $groupFilter)): ?>
                $('#group-list').append('<option selected class="group-option">' + '<?= $group ?>' + '</option>');
            <? else: ?>
                $('#group-list').append('<option class="group-option">' + '<?= $group ?>' + '</option>');
            <? endif; ?>
        <? endforeach; ?>
        <?if(in_array($arParams["MANAGER_GROUP_ID"], $arGroups)):?>
            <?if($arParams["ALLOW_VIEWING"] == "N"):?>

            <? foreach ($arResult['ONE_MANAGER'] as $manager): ?>
                <? if(in_array($manager['LAST_NAME'] . ' ' . $manager['NAME'], $managerFilter)): ?>
                    $('#manager-list').append('<option selected class="manager-option">' + '<?= $manager['LAST_NAME'] . ' ' . $manager['NAME'] ?>' + '</option>');
                <? else : ?>
                    $('#manager-list').append('<option class="manager-option">' + '<?= $manager['LAST_NAME'] . ' ' . $manager['NAME'] ?>' + '</option>');
                <? endif?>
            <? endforeach; ?>

            <?else:?>

            <? foreach ($arResult['MANAGERS'] as $manager): ?>
                <? if(in_array($manager['LAST_NAME'] . ' ' . $manager['NAME'], $managerFilter)): ?>
                    $('#manager-list').append('<option selected class="manager-option">' + '<?= $manager['LAST_NAME'] . ' ' . $manager['NAME'] ?>' + '</option>');
                <? else : ?>
                    $('#manager-list').append('<option class="manager-option">' + '<?= $manager['LAST_NAME'] . ' ' . $manager['NAME'] ?>' + '</option>');
                <? endif?>
            <? endforeach; ?>

            <?endif;?>
        <?else:?>
            <? foreach ($arResult['MANAGERS'] as $manager): ?>
                <? if(in_array($manager['LAST_NAME'] . ' ' . $manager['NAME'], $managerFilter)): ?>
                    $('#manager-list').append('<option selected class="manager-option">' + '<?= $manager['LAST_NAME'] . ' ' . $manager['NAME'] ?>' + '</option>');
                <? else : ?>
                    $('#manager-list').append('<option class="manager-option">' + '<?= $manager['LAST_NAME'] . ' ' . $manager['NAME'] ?>' + '</option>');
                <? endif?>
            <? endforeach; ?>
        <?endif;?>

        <? foreach ($arResult['SHOW'] as $show): ?>
            <? if(in_array($show, $showFilter)): ?>
                $('#show-list').append('<option selected class="show-option">' + '<?= $show ?>' + '</option>');
            <? else: ?>
                $('#show-list').append('<option class="show-option">' + '<?= $show ?>' + '</option>');
            <? endif ?>
        <? endforeach; ?>

        <? if(isset($dateFilter)): ?>
            $('#month-selector').val('<?= $dateFilter ?>');
        <? endif; ?>

        var a = document.getElementById('group-list');
        a.options[0].selected = true;
        a.options[1].selected = true;

        a.onclick = function () {
            a.options[0].selected = true;
            a.options[1].selected = true;
        }

    });
</script>