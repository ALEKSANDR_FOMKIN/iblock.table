<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

require_once "model/tables/Income.php";
require_once "model/tables/MonthPlans.php";
require_once "model/Statistics.php";
require_once "model/structures/Filter.php";

$APPLICATION->SetTitle("Учет плана продаж");

if(isset($_GET['login']) && $_GET['login'] == 'yes') {
    $actual_link = str_replace("?login=yes", '',"http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
    header("Location: $actual_link");
}

$incomes = array();

foreach ($arResult['INCOMES'] as $income) {
    $key = array_search($income[Income::MANAGER_KEY]['VALUE'], array_column($arResult['MANAGERS'], 'ID'));
    if($key !== false) {
        $income[Income::MANAGER_KEY]['VALUE'] = $arResult['MANAGERS'][$key]['NAME'] . ' ' . $arResult['MANAGERS'][$key]['LAST_NAME'];
        $income[Income::MANAGER_SWAPED_KEY]['VALUE'] = $arResult['MANAGERS'][$key]['LAST_NAME'] . ' ' . $arResult['MANAGERS'][$key]['NAME'];
    } else {
        $income[Income::MANAGER_KEY]['VALUE'] = 'НЕ НАЙДЕН';
    }
    $incomes[] = new Income($income);
}

usort($incomes, function($a, $b) {
    return strtotime($a->getDate()) - strtotime($b->getDate());
});

$plans = array();

foreach ($arResult['PLANS'] as $plan) {
    $key = array_search($plan[MonthPlans::MANAGER_KEY]['VALUE'], array_column($arResult['MANAGERS'], 'ID'));
    if($key !== false) {
        $plan[MonthPlans::MANAGER_KEY]['VALUE'] = $arResult['MANAGERS'][$key]['NAME'] . ' ' . $arResult['MANAGERS'][$key]['LAST_NAME'];
        $plan["swaped"]['VALUE'] = $arResult['MANAGERS'][$key]['LAST_NAME'] . ' ' . $arResult['MANAGERS'][$key]['NAME'];
    } else {
        $plan[MonthPlans::MANAGER_KEY]['VALUE'] = 'НЕ НАЙДЕН';
    }
    $plans[] = new MonthPlans($plan);
}

global $USER;
$user_id = $USER->GetID();
$last_name = $USER->GetLastName();
$name = $USER->GetFirstName();
$arGroups = CUser::GetUserGroup($user_id);
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once $_SERVER["DOCUMENT_ROOT"] . "/local/components/mcart/iblock.table/ExcelAPI/PHPExcel.php";

    $filter = new Filter($_POST['group_filter'], $_POST['manager_filter'], $_POST['show_filter'], $_POST['date_filter']);
    if (in_array($arParams["MANAGER_GROUP_ID"], $arGroups) && $arParams["ALLOW_VIEWING"] == "N") {
        $statistics = new Statistics($incomes, $plans, $arResult['GROUPS'], $arResult['ONE_MANAGER'], $arResult['SHOW'], $filter);
    }
    else{
        $statistics = new Statistics($incomes, $plans, $arResult['GROUPS'], $arResult['MANAGERS'], $arResult['SHOW'], $filter);
    }
} else {
    if(in_array($arParams["MANAGER_GROUP_ID"], $arGroups) && $arParams["ALLOW_VIEWING"] == "N") {
        $filter = new Filter(array(), array($last_name . " " . $name), array(), date('m.Y'));
        $statistics = new Statistics($incomes, $plans, $arResult['GROUPS'], $arResult['ONE_MANAGER'], $arResult['SHOW'], $filter);
    } else {
        $filter = new Filter(array(), array(), array(), date('m.Y'));
        $statistics = new Statistics($incomes, $plans, $arResult['GROUPS'], $arResult['MANAGERS'], $arResult['SHOW'], $filter);
    }
    //$statistics = new Statistics($incomes, $plans, $arResult['GROUPS'], $arResult['MANAGERS'], $arResult['SHOW'], $filter);
}

if(isset($_POST['get_incomes'])) {
    require_once 'excel/GetIncome.php';
} else if(isset($_POST['get_plans'])) {
    require_once 'excel/GetPlan.php';
} else if(isset($_POST['get_plan_fact_manager'])) {
    require_once 'excel/GetManagerStat.php';
} else if(isset($_POST['get_plan_fact_group'])) {
    require_once 'excel/GetGroupStat.php';
} else if(isset($_POST['get_plan_fact_show'])) {
    require_once 'excel/GetShowStat.php';
}

require_once "cdn.php";

if($_GET['p'] == 'excel') {
    require_once $_SERVER["DOCUMENT_ROOT"] . "/local/components/mcart/iblock.table/ExcelAPI/PHPExcel.php";
    require_once 'excel/GetIncome.php';
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $groupFilter = $_POST['group_filter'];
    $managerFilter = $_POST['manager_filter'];
    $showFilter = $_POST['show_filter'];
    $dateFilter = $_POST['date_filter'];
    if(!empty($arResult["ONE_MANAGER"]) && $arParams["ALLOW_VIEWING"] == "N"):
        $managerFilter = array($last_name . " " . $name);
    endif;
} else {
    $dateFilter = $filter->getDateFilter();
    if(!empty($arResult["ONE_MANAGER"]) && $arParams["ALLOW_VIEWING"] == "N"):
        $managerFilter = array($last_name . " " . $name);
    endif;
}

require_once "table.tpl";