var date = new Date();
$(function () {
    $('#month-selector').datepicker({
        autoclose: true,
        minViewMode: 1,
        format: 'mm.yyyy',
        language: 'ru',
        defaultViewDate: {
            year: date.getFullYear(),
        }
    });
});