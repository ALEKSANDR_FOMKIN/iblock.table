<?php
if(!CModule::IncludeModule("iblock"))
    return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch()) {
    $arIBlocks[$arRes["ID"]] = "[" . $arRes["ID"] . "] " . $arRes["NAME"];
}

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS" => array(
        "INCOMES_IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("INCOMES_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arTypesEx,
            "REFRESH" => "Y",
        ),
        "INCOMES_IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("INCOMES_IBLOCK_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "REFRESH" => "Y",
        ),
        "PLANS_IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("PLANS_IBLOCK_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "REFRESH" => "Y",
        ),
        "MANAGER_GROUP_ID" => array(
        "NAME" => GetMessage("MANAGER_GROUP_ID"),
        "TYPE" => "STRING",
        "DEFAULT" => $MANAGER_GROUP_ID,
        ),
        "ALLOW_VIEWING" => array(
            "NAME" => GetMessage("ALLOW_VIEWING"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
    )
);